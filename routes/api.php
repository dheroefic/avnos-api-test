<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'v1'], function () {
	Route::post('login', 'APIController@login');
	Route::group(['middleware' => 'auth:api'], function() {
		Route::post('logout', 'APIController@logout');
		Route::post('store', 'APIController@store');
		Route::post('show/{id}', 'APIController@show');
		Route::post('update/{id}', 'APIController@update');
		Route::post('delete/{id}', 'APIController@delete');
	});
});