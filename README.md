
# AVNOZ API TEST

## Objective:
    - Create REST API (LOGIN, LOGOUT, CREATE, READ, UPDATE, DELETE)
    - Authentication using JSON WEB TOKEN (JWT)
    - Limit the API request
    - Use PostgresSQL

## Setup:
    - Clone the project
    - composer install
    - unix / macos: cp .env-example .env
    - windows: copy .env-example .env
    - Change .env according to your environment
    - php artisan migrate --seed
    - php artisan passport:install
    - php artisan serve
    
## How-To:

### Login and get the token:
#### URI Invoke

    api/v1/login

#####  Header
 - 'Accept': application/json **(required)** 
##### Body
- user_name
- password
##### Output

    {
        "status": "OK",
        "message": "You have been logged in",
        "data": {
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyZTg0N2I3ZTVkMmZiZjQ0MDE1NTcwMmE5NTcxYTJjOWQ4MDk4NzZjNWZhMDA2NTUwZTU0YTQxY2RkOGUzNDg2OWNiNDQ3MGM5Y2Q1NGUyIn0.eyJhdWQiOiIxIiwianRpIjoiYTJlODQ3YjdlNWQyZmJmNDQwMTU1NzAyYTk1NzFhMmM5ZDgwOTg3NmM1ZmEwMDY1NTBlNTRhNDFjZGQ4ZTM0ODY5Y2I0NDcwYzljZDU0ZTIiLCJpYXQiOjE1NTU1OTIyMjcsIm5iZiI6MTU1NTU5MjIyNywiZXhwIjoxNTg3MjE0NjI3LCJzdWIiOiIxMSIsInNjb3BlcyI6W119.BS9JH6rEMNcNbJXHVCP5MzY_4QRF27ojTS4hJMO4CvDEzTMaYOwNfodbV8pL9WoMAZ7TbQGKaJYhKCB4S9TFfnySVMQXEz4_aGr3BK5kDIgL2PMF_F7ISICZRenXhg2AyMQ--0_eKHWatg3obmVjCQBD6Rc0uXx7D99O6JKCec4JEAUBdP2pFutsTWDPMgnbfELrkf8XBGrLGUMxWWmsPidG7T142hio8b-VRyTR6BACusf3bPeIWEgDVrSwMsfcP9Acw0rHQ6mecd1rWPtDbO_H_M_mbyQW5eD4hfYD-qKjyORSi_m--U06gDncmjrvu7M23GpGx5ArPDQyb2WUa3tf0s4geKd4r1TQA-R1EdKviqbSigqAxXlhDu79p6LDdjLev1Cg-cNngIclDgii8mpu1Dli7PvPn-v8YBXq7lBDG4uMzoVz4-Y65cckZuRtoJbzUcaBkDrE1fFa4I2YeZflRCEiyzgNTRHBCplQWMa6dxpxjCmQEzTzNuNLYkI8OypLM1IFHvuM_TVOeNcXE4jLv4ETLoFr1tLTgsbXmkyIwlpxVEHp_Ngz0f0meu2qUXJnjqkC7WgMe1ce2MTgBobg1phjN3WqmvXoGxR-n3MUj8BvU2iZp7359xgb-QW5YMAs2dQroyXdQ4waAF1gPdgiLUsXRzqDItKwQuGwZH8"
        }
    }

### Logout and revoke the token     
#### URI Invoke

    api/v1/logout

#####  Header
 - 'Accept': application/json **(required)** 
 - 'Authorization': Bearer ${TOKEN FROM LOGIN} **(required)**
##### Body
- none
##### Output                           

    {
        "status": "OK",
        "message": "You have been logged out"
    }

### Store the data
#### URI Invoke

    api/v1/store

#####  Header
 - 'Accept': application/json **(required)** 
 - 'Authorization': Bearer ${TOKEN FROM LOGIN} **(required)**
##### Body
 - user_name **(required)**
 - full_name **(required)**
 - email **(required)**
 - password **(required)**

##### Output

    {
        "status": "OK",
        "message": "User has been added"
    }
  
### Show the data 
#### URI Invoke

    api/v1/show/{id}

#####  Header
 - 'Accept': application/json **(required)** 
 - 'Authorization': Bearer ${TOKEN FROM LOGIN} **(required)**
##### Body
- none
##### Output

    {
        "status": "OK",
        "message": "Record found",
        "data": {
            "user": {
                "id": 1,
                "user_name": "vgrant",
                "full_name": "Mrs. Joanne Lesch PhD",
                "email": "amelia50@example.org"
            }
        }
    }

### Update the data
#### URI Invoke

    api/v1/update/{id}

#####  Header
 - 'Accept': application/json **(required)** 
 - 'Authorization': Bearer ${TOKEN FROM LOGIN} **(required)**

##### Body
 - user_name **(optional)** **(required if none in request)**
 - full_name **(optional)** **(required if none in request)**
 - email **(optional)** **(required if none in request)**
##### Output

    {
        "status": "OK",
        "message": "User has been updated"
    }

### Delete the data
#### URI Invoke

    api/v1/delete/{id}

#####  Header
 - 'Accept': application/json **(required)** 
 - 'Authorization': Bearer ${TOKEN FROM LOGIN} **(required)**
##### Body
- none
##### Output
    {
        "status": "OK",
        "message": "User has been deleted"
    }
  