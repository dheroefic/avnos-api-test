<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
    	for ($i = 0; $i < 10; $i++) {
    		$faker = Faker::create();
    		$user = new User();
    		$data = [
    			'user_name' => $faker->userName,
    			'full_name' => $faker->name,
    			'email' => $faker->unique()->safeEmail,
    			'email_verified_at' => now(),
    			'password' => Hash::make('demo')
    		];
    		$user->fill($data);
    		$user->save();
    	}
    }
}
