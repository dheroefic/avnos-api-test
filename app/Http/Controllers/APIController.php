<?php

namespace App\Http\Controllers;

// Library related goes here
use Illuminate\Http\Request;
use Auth;
use Hash;
// Model related goes here
use App\User;

class APIController extends Controller
{
	public function login(Request $request) {
		$user_name = $request->user_name;
		$password = $request->password;
		if ($user_name == '' && $password == '') return $this->responseBuilder('Failed', 401, 'Please input your credential');
		if (!Auth::attempt(['user_name' => $user_name, 'password' => $password])) {
			return $this->responseBuilder('Failed', 401, 'Your credential is invalid');
		}
		return $this->responseBuilder('OK', 200, 'You have been logged in', [
			'token' => Auth::user()->createToken('api-test')->accessToken
		]);
	}

	public function logout() {
		$accessToken = Auth::user()->token();
		$accessToken->revoke();
		return $this->responseBuilder('OK', 200, 'You have been logged out');
	}

	public function store(Request $request) {
		$user_name = $request->user_name;
		$full_name = $request->full_name;
		$email = $request->email;
		$password = $request->password;
		if (
			$user_name == '' &&
			$full_name == '' &&
			$email == '' &&
			$password == ''
		) return $this->responseBuilder('Failed', 401, 'Please input all of the required field');
		$user = new User();
		if ( !empty(User::where(['email' => $email])->first()) ) return $this->responseBuilder('Failed', 401, 'User has been added before');
		$data = [
			'user_name' => $user_name,
			'full_name' => $full_name,
			'email' => $email,
			'password' => Hash::make($password)
		];
		$user->fill($data);
		$user->save();
		return $this->responseBuilder('OK', 200, 'User has been created');
	}

	public function show($id) {
		$user = User::where('id', $id)->first();
		return empty($user) ? 
		$this->responseBuilder('OK', 200, 'Record not found') : 
		$this->responseBuilder('OK', 200, 'Record found', ['user' => $user]);
	}

	public function update($id, Request $request) {
		$user_name = $request->user_name;
		$full_name = $request->full_name;
		$email = $request->email;
		if (
			$user_name == '' &&
			$full_name == '' &&
			$email == ''
		) return $this->responseBuilder('Failed', 401, 'At least one field is present in the request');
		$user = User::where('id', $id)->first();
		if (empty($user)) $this->responseBuilder('OK', 200, 'Record not found');
		if ($user_name != '') $user->user_name = $user_name;
		if ($full_name != '') $user->full_name = $full_name;
		if ($email != '') $user->email = $email;
		$user->save();
		return $this->responseBuilder('OK', 200, 'User has been updated');
	}


	public function delete($id) {
		User::find($id)->delete();
		return $this->responseBuilder('OK', 200, 'User has been deleted');
	}
}
