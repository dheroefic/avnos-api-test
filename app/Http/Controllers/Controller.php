<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Response builder for REST API
     * @param String $status 
     * @param int $statusCode 
     * @param String $message 
     * @param array $data 
     * @return response
     */
    protected function responseBuilder($status, $statusCode=200, $message, $data=[]) {
    	$response = [
    		'status' => $status,
    		'message' => $message
    	];
    	if (count($data) > 0) $response['data'] = $data;
    	return response()->json($response, $statusCode);
    }
}
